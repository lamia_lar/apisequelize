const Joi = require("joi");

const productValidation = (body) => {
    const ProductSchema =Joi.object().keys( {
        title : Joi.string().min(3).max(40).required(),
        price : Joi.number().integer().required(),
        description : Joi.string().min(3).max(50)
    });
    
    //return ProductSchema.validate(body)
    const Validation = ProductSchema.validate(body);
    
   
}

module.exports = productValidation