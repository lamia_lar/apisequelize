const Product = require('../models/product.js')
// validation product
const productValidation =require('../validation/productValidation.js')


const getAll = (req, res) => {
    Product.findAll ()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send ({
            message: err.message || "une erreur s'est produite lors de la récupération des produits "
        });
})
    /*console.log('exportsfindAll')
    const title = req.query.title;
    var condition = title ? {title:{ [Op.like]: `%${title}%`}} : null;
    Product.findAll({where : condition})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send ({
            message: err.message || "une erreur s'est produite lors de la récupération des produits "
        });
}
    )*/
}
const getOne = (req, res) => {
    const {id} = req.params
    Product.findByPk(id) 
    .then(product => {
        if(!product) return res.status(404).json({msg :"Produit non défini"})
        res.status(200).json(product)
    })
    .catch(error => res.status(500).json(error))
};


const createOne = (req, res) => {
    const {body} = req
    //validation
    const error = productValidation(body)

    const errorBis ={...error}
    console.log(errorBis);
    if (error) return res.status(401).json(error.details[0].message)
    
    Product.create({...body})
    .then(() => {
        res.status(201).json({msg: "crée avec succes"})
    })
    .catch(error => res.status(500).json(error.parent))
}   

const updateOne = (req, res) => {
    const {id} = req.params
    const {body} = req;

  Product.findByPk(id)
  .then(product => {
    if(!product) return res.status(201).json({msg :  "le Produit n'existe pas"})
    product.title = body.title
    product.save()
    .then(() => res.status(201).json({ msg : "Mise à jour effectuée"}))
    .catch((error) => res.status(500).json(error));
  })
};
const deleteOne = (req, res) => {
    const {id} = req.params
    Product.destroy({where : {id : id}})
    .then(ressource => {
        if(ressource ===0) return res.status(404).json( { msg :"le Produit n'existe pas"})
        res.status(200).json({msg :"Produit supprimé"})
    })
    .catch((error) => res.status(500).json(error))
};

module.exports =
 { getAll, getOne, createOne, updateOne, deleteOne };
