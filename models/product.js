const { Sequelize, DataTypes } = require("sequelize");
const Config = require('../Config/db.js');
const sequelize = require('../Config/db.js')

const Product = sequelize.define("product", {
  id: {
    type:DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false
  },
  title:{
    type: DataTypes.STRING,
    allowNull : false,
  },
  price :{
    type : DataTypes.INTEGER,
    
    allowNull :true,
  },
  description :{
    type : DataTypes.TEXT,
    allowNull : true
  }
});


module.exports = Product