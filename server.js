const express = require('express');
const logger = require("./Config/logger.js");
const app = express();
const routes = require('./routes/routes.js')
const config = require('./Config/db.js')
const sequelize = require('sequelize')

app.use(express.json())
app.use(routes)

app.get('/',(req, res) => {
    res.json({message: 'Hello from api'})

})


config.sync()
.then((console.log('connexion à la bdd')))
.catch(error => console.log(error.message))



const PORT = process.env.PORT || 8080

app.listen(PORT, () => {
    logger.log('info',`Serveur est connecté au port ${PORT}`)
});